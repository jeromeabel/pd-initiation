# Pure Data Initiation

Atelier de découverte du langage Pure Data

## Installation
- Logiciel Pure Data 0.52.1 : http://msp.ucsd.edu/software.html
- Installer Else : Menu Aide > Installer des objets supplémentaires : else

## Inspirations
- historique : adapté de Hans Christoph Steiner

## Notes 
- Grace Hopper : ENIAC, compilateur, FLOW-MATIC(COBOL)
- Max Mathews : ADC/DAC, PCM samples
- MUSIC-N : environnement modulaire composé de blocks (oscillateurs, enveloppes, mélangeurs...) qui peuvent être reliés les uns aux autres.
- MUSIC vs Synthétiseurs modulaires analogiques Moog et Buchla 1964 - 1980: "I wouldn't say that I copied the analog synthesizer building blocks. I think we actually developed them fairly simultaneously." (Max Mathews)
- "« La musique a toujours été limitée acoustiquement par les instruments car leurs mécanismes ont des restrictions physiques. Nous avons produit des sons et de la musique, directement à partir de nombres, surmontant ainsi, les limites conventionnelles des instruments. Ainsi, l'univers musical n'est maintenant plus circonscrit que par nos perceptions, et notre créativité. » Max Mathews dans la revue musicale allemande Gravesaner blätter , 1962.
- Max Mathews explique : « Vingt minutes de temps de calculs d'ordinateur sont nécessaires pour chaque seconde de son produit, ce doit être une seconde remarquable pour que cet effort en vaille la peine. 

## Liens
- https://www.stereolux.org/blog/histoire-des-arts-informatiques-ndeg1-le-jour-ou-ibm-chanta
- https://www.personal.psu.edu/users/m/e/meb26/INART55/bell_labs.html : histoire des Bell Labs et de Max Mathews
- http://msp.ucsd.edu/tools/reality/elind-pma-msp-prop.pdf : histoire des débuts de Max/Pd à l'IRCAM
- http://msp.ucsd.edu/Publications/pdf_EMS14_puckette.pdf
- https://www.musicainformatica.org/topics/miller-puckette.php : histoire de Miller Puckette
- https://industrie-culturelle.fr/industrie-culturelle/la-musique-informatique-jean-baptiste-favory/ : La musique informatique – Jean-Baptiste FAVORY

## Sources des images
- eniac.gif : http://histoire.info.online.fr/images/eniac5.jpeg
- inout.gif : pdf de Nicolas Montgermont
- java.gif : Jérôme Abel
- langage-all.gif : Jérôme Abel
- onde-en-c.gif : "Designing Sound", FARNELL Andy, Designing Sound, Applied Scientific Press, 2010
- pd.gif : http://fr.wikipedia.org/wiki/Fichier:PureData_icone.jpg
